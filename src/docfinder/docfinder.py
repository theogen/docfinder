#!/usr/bin/env python

import sys
import subprocess
from pathlib import Path
from fuzzysearch import find_near_matches

class SearchSettings:
    path = Path()
    terms = []
    exclude_paths = []
    case_sensitive = False
    search_by_filename = True
    search_by_content = True
    literal_search = True
    max_substitutions = None
    max_insertions = None
    max_deletions = None
    max_l_dist = 1

class Match:
    def __init__(self, term, start, end, matched):
        self.term = term
        self.start = start
        self.end = end
        self.matched = matched


def __popen_output(args:list):
    p = subprocess.Popen(args, stdout=subprocess.PIPE)
    return p.stdout.read().decode(encoding='utf-8')

def __doc2txt(path):
    return __popen_output(['catdoc', path])

def __docx2txt(path):
    return __popen_output(['docx2txt', path, '-']).replace('\n', '\n\n')

def __odt2txt(path):
    return __popen_output(['odt2txt', '--width=-1', path])

EXTENSIONS = {'.doc' : __doc2txt, '.docx' : __docx2txt, '.odt' : __odt2txt}

def findstr(settings:SearchSettings, progress=None):
    results = []
    files = list(settings.path.rglob('*'))
    i = 0
    for p in files:
        if progress: progress(i + 1, len(files))
        i += 1
        if not p.suffix in EXTENSIONS.keys() and settings.search_by_content:
            continue
        excluded = False
        for exclude in settings.exclude_paths:
            if exclude in str(p):
                excluded = True
                break
        if excluded:
            continue
        txt = ''
        if settings.search_by_filename:
            txt += str(p.stem) + '\n\n'
        if settings.search_by_content:
            txt += EXTENSIONS[p.suffix](p)
        result = {'path' : p, 'text' : txt, 'matches' : []}
        for term in settings.terms:
            limits = (settings.max_substitutions,
                      settings.max_insertions,
                      settings.max_deletions,
                      settings.max_l_dist)
            if settings.literal_search:
                limits = (0, 0, 0, 0)
            nm = find_near_matches(
                term.lower() if not settings.case_sensitive else term,
                txt.lower() if not settings.case_sensitive else txt,
                *limits)

            matches = []
            for m in nm:
                matches.append(Match(term, m.start, m.end, m.matched))

            result['matches'] += matches
        if len(result['matches']) > 0:
            result['matches'].sort(key=lambda m: m.start)
            results.append(result)
    return results


if __name__ == '__main__':
    settings = SearchSettings()
    settings.path = Path(sys.argv[1])
    settings.terms = sys.argv[2:]
    results = findstr(settings)
    for r in results:
        print(r['path'])
