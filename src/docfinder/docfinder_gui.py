#!/usr/bin/env python

import os
import PySimpleGUI as sg
import pyperclip
import subprocess
import json
from pathlib import Path
try:
    from docfinder import docfinder
except ImportError:
    import docfinder

########################################################################
# CONSTANTS
########################################################################

XDG_CONFIG_HOME = Path(os.getenv('XDG_CONFIG_HOME')
                       or os.getenv('HOME') + '/.config')
CONFIG_DIR = XDG_CONFIG_HOME / 'docfinder'
CONFIG_FILE = CONFIG_DIR / 'config.json'

DEFAULT_CONFIG = {
    'path' : '',
    'exclude' : '',
    'search_by_filename' : True,
    'search_by_content' : True,
    'literal_search' : False,
    'max_l_dist' : 1,
    'show_full_path' : False,
}

########################################################################
# FUNCTIONS
########################################################################

def load_config() -> dict:
    # Loading config
    os.makedirs(CONFIG_DIR, exist_ok=True)
    config = DEFAULT_CONFIG.copy()
    try:
        with open(CONFIG_FILE, 'rb') as f:
            config.update(json.load(f))
    except FileNotFoundError:
        pass
    return config


def progress(i:int, total:int):
    progressbar.update(current_count=i, max=total)
    window.refresh()


def name(name:str, tooltip:str = None, width:int=16):
    return sg.Text(name, s=(width, 1), tooltip=tooltip)


def can_search() -> bool:
    return len(inputbox.get()) != 0 and \
    (window['search_by_content'].get() or \
    window['search_by_filename'].get())


def get_settings() -> docfinder.SearchSettings:
    settings = docfinder.SearchSettings()
    settings.path = Path(window['path'].get())
    settings.terms = inputbox.get().split(',')
    settings.search_by_content = window['search_by_content'].get()
    settings.search_by_filename = window['search_by_filename'].get()
    settings.literal_search = window['literal_search'].get()
    settings.max_l_dist = max_l_dist
    return settings


def search(settings):
    listbox.update(values=[])
    multiline.update('')
    window['open'].update(disabled=True)
    window['open_folder'].update(disabled=True)
    window['copy'].update(disabled=True)
    window['prev'].update(disabled=True)
    window['next'].update(disabled=True)
    window['search'].update(disabled=True)
    window.refresh()
    if window['exclude'].get():
        settings.exclude_paths = window['exclude'].get().split(';')
    print('searching terms %s in %s' % (settings.terms, settings.path))
    results = docfinder.findstr(settings, progress)
    show_results_list(results, settings, window['show_full_path'].get())
    window['search'].update(disabled=False)
    return results


def show_results_list(results, settings, show_full_path, index=0):
    paths = []
    for r in results:
        if show_full_path:
            paths.append(str(r['path'].relative_to(settings.path)))
        else:
            paths.append(str(r['path'].name))
    listbox.update(paths, set_to_index=index)


def show_result(result):
    multiline.update('')
    pos = 0
    i = 0
    for m in result['matches']:
        multiline.update(result['text'][pos:m.start], append=True)
        multiline.update(result['text'][m.start:m.end],
                         text_color_for_value='red',
                         background_color_for_value='yellow', append=True)
        pos = m.end
        i += 1
    multiline.update(result['text'][pos:], append=True)
    window['open'].update(disabled=False)
    window['open_folder'].update(disabled=False)
    window['copy'].update(disabled=False)
    window['prev'].update(disabled=False)
    window['next'].update(disabled=False)


def scroll_to_match(result, match_id):
    multiline.set_focus()
    index = result['matches'][match_id].start
    multiline.Widget.mark_set('insert', "1.0+%d chars" % index)
    multiline.Widget.see("1.0+%d chars" % index)
    window['match_text'].update(
        'Совпадение %d из %d' % (match_id + 1, len(result['matches'])))


########################################################################
# MAIN
########################################################################

config = load_config()

# Layout
ll = [[name('Искать в папке'),
       sg.Input(config['path'], k='path', s=(15,10)), sg.FolderBrowse()],

      [name('Исключить папки', 'Используйте ; для разделения'),
       sg.Input(config['exclude'], expand_x=True, k='exclude')],

      [name('Ключевые слова', 'Перечисляйте слова через запятую ,'),
       sg.Input(expand_x=True, k='terms', enable_events=True)],

      [sg.Checkbox('Пойск в имени файла', config['search_by_filename'],
                   k='search_by_filename', enable_events=True)],
      [sg.Checkbox('Пойск в содержимом файлов', config['search_by_content'],
                   k='search_by_content', enable_events=True)],
      [sg.Checkbox('Буквальный пойск', config['literal_search'],
                   k='literal_search', enable_events=True)],
      [name('Допустимая неточность', width=22),
       sg.Slider((0,10), config['max_l_dist'], enable_events=True,
                 orientation='h', expand_x=True, s=(10,10), k='max_l_dist')],

      [sg.ProgressBar(100, size=(50, 20), k='progressbar', expand_x=True)],

      [sg.Button('Пойск', k='search', disabled=True),
       sg.Button('Выход', k='cancel')]]

lc = [[sg.Multiline(s=(90,30), expand_x=True, expand_y=True,
                    k='multiline', font=("Helvetica", 14))],
      [sg.Button('Пред', k='prev', disabled=True),
       sg.Button('След', k='next', disabled=True),
       sg.Text('Совпадение 0 из 0', key='match_text')]]

lr = [[sg.Listbox([], s=(50,30), expand_x=True, expand_y=True,
                  k='listbox', enable_events=True, horizontal_scroll=True)],
      [sg.Button('Откр.', k='open', disabled=True),
       sg.Button('Откр. папку', k='open_folder', disabled=True),
       sg.Button('Коп. путь', k='copy', disabled=True),
       sg.Checkbox('Отобр. полный путь', config['show_full_path'],
                   k='show_full_path', enable_events=True)]]

layout = [[
    sg.Col(ll, p=0, expand_x=False, expand_y=True, size=(320, 800)),
    sg.Col(lc, p=0, expand_x=True, expand_y=True),
    sg.Col(lr, p=0, expand_x=True, expand_y=True)]]

# Create the Window
window = sg.Window('DOC Finder', layout, finalize=True, resizable=True,
                   auto_size_text=True, auto_size_buttons=True)
inputbox = window['terms']
listbox = window['listbox']
multiline = window['multiline']
progressbar = window['progressbar']
results = []
max_l_dist = int(config['max_l_dist'])
cursor_pos = 0
last_settings = None

window.set_min_size((1680, 720))

# Autofocus on search field
inputbox.set_focus()

# Event Loop to process "events" and get the "values" of the inputs
while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED or event == 'cancel':
        break
    if event == 'terms':
        window['search'].update(disabled=not can_search())
    if event == 'search':
        last_settings = get_settings()
        results = search(last_settings)
    if event == 'listbox':
        if len(listbox.get_indexes()) == 0:
            continue
        result = results[listbox.get_indexes()[0]]
        show_result(result)
        cursor_pos = 0
        scroll_to_match(result, 0)
    if event == 'copy':
        result = results[listbox.get_indexes()[0]]
        pyperclip.copy(str(result['path']))
    if event == 'open':
        result = results[listbox.get_indexes()[0]]
        subprocess.Popen(['libreoffice', str(result['path'])])
    if event == 'open_folder':
        result = results[listbox.get_indexes()[0]]
        subprocess.Popen(['xdg-open', str(result['path'].parent)])
    if event == 'search_by_filename' or event == 'search_by_content':
        window['search'].update(disabled=not can_search())
    if event == 'max_l_dist':
        max_l_dist = int(values['max_l_dist'])
    if event == 'next' or event == 'prev':
        result = results[listbox.get_indexes()[0]]
        if event == 'next':
            cursor_pos += 1
            if cursor_pos == len(result['matches']):
                cursor_pos = 0
        elif event == 'prev':
            cursor_pos -= 1
            if cursor_pos == -1:
                cursor_pos = len(result['matches']) - 1
        scroll_to_match(result, cursor_pos)
    if event == 'show_full_path':
        if not last_settings:
            last_settings = get_settings()
        show_results_list(results, last_settings,
                          window['show_full_path'].get(),
                          listbox.get_indexes()[0])


# Writing config
config['path'] = window['path'].get()
config['exclude'] = window['exclude'].get()
config['search_by_filename'] = window['search_by_filename'].get()
config['search_by_content'] = window['search_by_content'].get()
config['literal_search'] = window['literal_search'].get()
config['max_l_dist'] = max_l_dist
config['show_full_path'] = window['show_full_path'].get()
with open(CONFIG_FILE, 'w', encoding='utf-8') as f:
    json.dump(config, f, ensure_ascii=False, indent=4)
window.close()
