# DOC Finder

A simple GUI program for searching DOC, DOCX and ODT files made with
PySimpleGUI.

It uses `catdoc`, `docx2txt` and `odt2txt` under the hood.

![A screenshot of the program](./assets/screenshot.png)
